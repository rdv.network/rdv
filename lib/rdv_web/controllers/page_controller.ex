defmodule RdvWeb.PageController do
  use RdvWeb, :controller

  def index(conn, _params) do
    # user = Guardian.Plug.current_resource(conn)
    # render(conn, "index.html", current_user: user)
    render(conn, "index.html")
  end

  def protected(conn, _) do
    user = Guardian.Plug.current_resource(conn)
    render(conn, "protected.html", current_user: user)
  end
end
