defmodule Rdv.Repo do
  use Ecto.Repo,
    otp_app: :rdv,
    adapter: Ecto.Adapters.Postgres
end
