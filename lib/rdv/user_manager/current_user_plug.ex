defmodule Rdv.UserManager.CurrentUserPlug do
  import Plug.Conn

  def init(options), do: options

  def call(conn, _opts) do
    conn
    |> assign(:current_user, Guardian.Plug.current_resource(conn))
  end
end
