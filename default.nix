with import <nixpkgs> {};
{
    mix_env = stdenv.mkDerivation {
        name = "mix_sandbox";
        buildInputs = [
          pkgs.stdenv
          pkgs.gnum4
        ];
    };
}
