# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :rdv,
  ecto_repos: [Rdv.Repo]

# Configures the endpoint
config :rdv, RdvWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "fPURPg/4Qo/JSRJVomaGkYd0bwxKVJpt1mcRKOnmUzRpQ3zbBR+ML8DWo70qxnsT",
  render_errors: [view: RdvWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Rdv.PubSub, adapter: Phoenix.PubSub.PG2]

config :rdv, Rdv.UserManager.Guardian,
  issuer: "auth_me",
  # put the result of the mix command above here
  secret_key: "DLEhxEPWclUFuxsn9+q2fO5gHHXV3e8UxoKjhCWsdVlPu5xTm4f6aOZ8KxQYFkou"

config :ueberauth, Ueberauth,
  providers: [
    github: {Ueberauth.Strategy.Github, []}
  ]

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID"),
  client_secret: System.get_env("GITHUB_CLIENT_SECRET")

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
